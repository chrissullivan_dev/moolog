# Moo

### What is moo?

Moo is a simple logging system for Node Servers. It will log the day in ISO, the request method and the url hit and save them based on day.

### How do I add it to my project?

```javascript
npm install moolog --save
```

### How do I configure it?

Since its my first module and I wanted to keep the complexity down to a minimum, you have two options for configuration.

```javascript
var moolog = require('moolog');

var options = {
	re: /.*(js|ico)/, // ignore any logs matching this re 
	display: true,   // show the logs as they are processed
	path: 'myLogs'   // a directory name to store logs in
};
app.use( moolog( options ) );
```


