
'use strict'

const fs = require('fs');

module.exports = moolog

function moolog( options ) {
	var opts    = options || {}
	var path    = opts.path || 'log'
	var log     = fs.existsSync( path ) || fs.mkdirSync( path );	
	var re 	    = opts.re || /.*(js|ico)/
	return function( req, res, next ) {
		if( !re.test( req.url ) ) {

			var today = new Date().toISOString();
			var msg = [
				today,
				req.method,
				req.url
			].join(' ');

			// assume directory exists based on path value
			if( path ) {
       				fs.appendFile( path + '/' + today.slice(0, 10), msg + '\n', (err) => { 
					if( err ) console.log( err );
				});
			}
	
			// output server request
			if( opts.display ) 
			{ 
				console.log( msg );
			}	

		}
		next();
	}
}



